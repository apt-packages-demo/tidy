# [tidy](https://tracker.debian.org/pkg/tidy-html5)

HTML/XML syntax checker and reformatter http://www.html-tidy.org/

Unofficial howto and demo

* https://en.wikipedia.org/wiki/HTML_Tidy

## Other distributions
* Not yet in Alpine https://pkgs.alpinelinux.org/packages?name=tidy

## License
The tidy license has some similarities with https://choosealicense.com/licenses/zlib/